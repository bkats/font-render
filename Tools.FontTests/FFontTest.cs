﻿using Xunit;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Tools.Font;
using System;

namespace ToolsFreeType.Tests
{
    public class FFontTests
    {
        [Fact()]
        public void StartMeasureTest()
        {
            var fw = new FreeTypeWrapper();
            fw.SetDPI(96);
            using var file = File.OpenRead(@"D:\games\LeapofFaith-0.4.51c-pc\game\Museo500.otf");
            var face = fw.LoadFont(file);
            var ms = face.StartMeasure(16);
            var box = ms.GetBoundBox();
            Assert.Equal(1, box.Width);
            Assert.Equal(25, box.Height);
            ((IDisposable)face).Dispose();
        }

        [Fact()]
        public void MeasureNextCharTest()
        {
            var fw = new FreeTypeWrapper();
            fw.SetDPI(96);
            using var file = File.OpenRead(@"D:\games\LeapofFaith-0.4.51c-pc\game\Museo500.otf");
            var face = fw.LoadFont(file);
            var ms = face.StartMeasure(16);
            
            face.MeasureNextChar(ms, 'A');
            
            var box = ms.GetBoundBox();
            Assert.Equal(14, box.Width);
            Assert.Equal(25, box.Height);
            ((IDisposable)face).Dispose();
        }

        [Fact]
        public void MeasureNextCharGetBoxesTest()
        {
            var fw = new FreeTypeWrapper();
            fw.SetDPI(96);
            using var file = File.OpenRead(@"D:\games\LeapofFaith-0.4.51c-pc\game\Museo500.otf");
            var face = fw.LoadFont(file);
            var ms = face.StartMeasure(16);

            face.MeasureNextChar(ms, 'A');
            face.MeasureNextChar(ms, 'B');

            var box = ms.GetBoundBox();
            Assert.Equal(27, box.Width);
            Assert.Equal(25, box.Height);

            var boxes = ms.GetCharacterBoxes();
            Assert.Equal(2, boxes.Count);
            ((IDisposable)face).Dispose();
        }

        [Fact]
        public void RenderTextTest()
        {
            var fw = new FreeTypeWrapper();
            fw.SetDPI(96);
            using var file = File.OpenRead(@"D:\TestOutput\cambriab.ttf");
            var face = fw.LoadFont(file);

            var bitmap = new WriteableBitmap(16,16,96,96,PixelFormats.Bgra32,null);
            bitmap.Lock();
            var box = new BoundBox(0, 0, 16, 16);
            face.RenderText(box, 1, 12, "A", 12, bitmap.BackBuffer, bitmap.BackBufferStride);
            bitmap.Unlock();

            using var stream5 = new FileStream(@"D:\testoutput\A.png", FileMode.Create);
            var encoder5 = new PngBitmapEncoder();
            encoder5.Frames.Add(BitmapFrame.Create(bitmap));
            encoder5.Save(stream5);
            ((IDisposable)face).Dispose();
        }
    }
}
