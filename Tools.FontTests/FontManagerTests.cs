﻿using Xunit;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace Tools.Font.Tests
{
    public class FontManagerTests
    {
        [Fact()]
        public void LoadFontTest()
        {
            using var file = File.OpenRead(@"D:\games\LeapofFaith-0.4.51c-pc\game\Museo500.otf");
            Assert.True(FontManager.LoadFont("Test1", file), "Failed to load font");
        }

        [Fact()]
        public void DrawTextTest()
        {
            using var file = File.OpenRead(@"D:\games\LeapofFaith-0.4.51c-pc\game\Museo500.otf");
            FontManager.LoadFont("Test2", file);
            var bitmap = new WriteableBitmap(640, 480, 96, 96, PixelFormats.Bgra32, null);
            var r = new Rect(5, 5, 630, 470);
            Assert.True(FontManager.DrawText(bitmap, r, "Test2", "the quick brown fox jumps over the lazy dog. 0123456789 !@#$%^&*()", 20, Colors.AntiqueWhite));

            using var stream5 = new FileStream(@"D:\testoutput\test.png", FileMode.Create);
            var encoder5 = new PngBitmapEncoder();
            encoder5.Frames.Add(BitmapFrame.Create(bitmap));
            encoder5.Save(stream5);
        }

        [Fact()]
        public void ClearFontsTest()
        {
            using var file = File.OpenRead(@"D:\games\LeapofFaith-0.4.51c-pc\game\Museo500.otf");
            FontManager.LoadFont("Test3", file);
            Assert.True(FontManager.HasFont("Test3"));
            FontManager.ClearFonts();
            Assert.False(FontManager.HasFont("Test3"));
        }

        [Fact()]
        public void MeasureTextTest()
        {
            var txt = "Help, mijn man is klusser";

            using var file = File.OpenRead(@"D:\games\LeapofFaith-0.4.51c-pc\game\Museo500.otf");
            FontManager.LoadFont("Test4", file);
            var s = FontManager.MeasureText("Test4", txt, 12);
            Assert.InRange(s.Width, 180, 195);
            Assert.InRange(s.Height, 20, 22);

            var bitmap = new WriteableBitmap((int)s.Width, (int)s.Height, 96, 96, PixelFormats.Bgra32, null);
            var r = new Rect(0, 0, s.Width, s.Height);
            Assert.True(FontManager.DrawText(bitmap, r, "Test4", txt, 12, Colors.AntiqueWhite));

            using var stream5 = new FileStream(@"D:\testoutput\test_measure.png", FileMode.Create);
            var encoder5 = new PngBitmapEncoder();
            encoder5.Frames.Add(BitmapFrame.Create(bitmap));
            encoder5.Save(stream5);
        }

        [Fact()]
        public void DrawFormatedTextTest()
        {
            using var file = File.OpenRead(@"D:\games\LeapofFaith-0.4.51c-pc\game\Museo500.otf");
            FontManager.LoadFont("Test5", file);

            var text = "This line is long enough to be split in multiple lines.\nHopefully it works as expected."; 

            var bitmap = FontManager.DrawFormatedText(text, "Test5", 16, Colors.CornflowerBlue, 320);

            using var stream5 = new FileStream(@"D:\testoutput\formatted.png", FileMode.Create);
            var encoder5 = new PngBitmapEncoder();
            encoder5.Frames.Add(BitmapFrame.Create(bitmap));
            encoder5.Save(stream5);
        }
    }
}