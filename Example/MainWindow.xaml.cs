﻿using System.IO;
using System.Windows;
using System.Windows.Media;
using Tools.Font;

namespace Example
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly string loremIpsum =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus libero leo, pellentesque ornare, " +
    "adipiscing vitae, rhoncus commodo, nulla. Fusce quis ipsum. Nulla neque massa, feugiat sed, commodo " +
    "in, adipiscing ut, est. In fermentum mattis ligula. Nulla ipsum. Vestibulum condimentum condimentum " +
    "augue. Nunc purus risus, volutpat sagittis, lobortis at, dignissim sed, sapien. Fusce porttitor " +
    "iaculis ante. Curabitur eu arcu. Morbi quam purus, tempor eget, ullamcorper feugiat, commodo " +
    "ullamcorper, neque.";

        public ImageSource Example { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            FontManager.LoadFont("TestFont", File.OpenRead(@"D:\games\LeapofFaith-0.4.51c-pc\game\Museo500.otf"));
            Example = FontManager.DrawFormatedText(loremIpsum, "TestFont", 16, Colors.Black, 400);
            DataContext = this;
        }
    }
}
