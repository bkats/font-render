#pragma once

using namespace System;
using namespace System::Collections::Generic;
using namespace System::IO;
using namespace Tools::Font;

extern "C" {
	#include <ft2build.h>
	#include FT_FREETYPE_H
}

namespace ToolsFreeType {
	public ref class FreeTypeWrapper : IFontLoader
	{
	public:
		FreeTypeWrapper();

		~FreeTypeWrapper()
		{
			if (m_isDisposed)
				return;
			this->!FreeTypeWrapper();
			m_isDisposed = true;
			System::GC::SuppressFinalize(this);
		}

		!FreeTypeWrapper();

		virtual IFontFace^ LoadFont(Stream^ s);

		virtual void SetDPI(int dpi);
	internal:
		FT_Library* library;
	private:
		int dpi;
		bool m_isDisposed;
	};
}
