#include "pch.h"
#include "FreeTypeWrapper.h"
#include "FFont.h"

namespace ToolsFreeType {

    FreeTypeWrapper::FreeTypeWrapper()
    {
        m_isDisposed = false;
        library = (FT_Library*)malloc(sizeof(FT_Library));
        if (FT_Init_FreeType(library) != FT_Err_Ok) {
            throw gcnew FontRenderException("Failed to initialize FreeType library");
        }
        dpi = 96;
    }

    FreeTypeWrapper::!FreeTypeWrapper()
    {
        if (library != NULL)
        {
            FT_Done_FreeType(*library);
            free(library);
        }
        library = NULL;
    }

    IFontFace^ FreeTypeWrapper::LoadFont(Stream^ s)
    {
        FFont^ face = gcnew FFont(this, s, dpi);
        return face;
    }

    void FreeTypeWrapper::SetDPI(int dpi)
    {
        this->dpi = dpi;
    }
}