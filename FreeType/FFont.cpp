#include "pch.h"
#include "FFont.h"
#include "Measure.h"

// The following code is based on the tutorial from FreeType:
// https://freetype.org/freetype2/docs/tutorial/index.html

static void Check(FT_Error error)
{
    if (error) {
        const char* msg = FT_Error_String(error);
        if (msg != NULL) {
            throw gcnew FontRenderException(gcnew String(msg));
        }
        throw gcnew FontRenderException(String::Format("FreeType error: {0}", error));
    }
}

ToolsFreeType::FFont::FFont(FreeTypeWrapper^ lib, Stream^ fs, int dpi)
{
    m_isDisposed = false;
    library = lib;
    face = new FT_Face;
    this->dpi = dpi;

    int size = (int)fs->Length;
    fontData = (FT_Byte*)malloc(size);
    UnmanagedMemoryStream target(fontData, size, size, FileAccess::Write);
    fs->CopyTo(%target);

    Check(FT_New_Memory_Face(*(lib->library), fontData, size, 0, face));
}

ToolsFreeType::FFont::!FFont()
{
    if (face != NULL)
    {
        FT_Done_Face(*face);
        delete face;
    }
    face = NULL;
    
    if (fontData != NULL) 
    {
        free(fontData);
    }
    fontData = NULL;
}

int ToolsFreeType::FFont::ToPixels(short value, int fontSize) {
    // Convert EM unit based value to current DPI and font size
    return (value * fontSize * dpi) / ((*face)->units_per_EM * 72);
}

IMeasure^ ToolsFreeType::FFont::StartMeasure(int size)
{
    Check(FT_Set_Char_Size(*face, size * 64, 0, dpi, 0));

    Measure^ m = gcnew Measure();

    m->use_kerning = FT_HAS_KERNING(*face);

    // Start measure box with 1 pixel width 
    m->bbox->xMin = 0;
    m->bbox->xMax = 1;
    // and expected line height based on ascender and descender
    m->bbox->yMin = ToPixels((*face)->descender, size);
    m->bbox->yMax = ToPixels((*face)->ascender, size);;

    // Measuring from (0, 0) 
    m->pen->x = 0;
    m->pen->y = 0;

    return m;
}

void ToolsFreeType::FFont::MeasureNextChar(IMeasure^ measure, Char c)
{
    FT_UInt  glyph_index;
    FT_BBox  glyph_bbox;
    Measure^ m = (Measure^)measure;

    glyph_index = FT_Get_Char_Index(*face, c);

    // retrieve kerning distance and move pen position
    if (m->use_kerning && m->previous && glyph_index)
    {
        FT_Vector  delta;
        FT_Get_Kerning(*face, m->previous, glyph_index, FT_KERNING_DEFAULT, &delta);
        m->pen->x += delta.x;
    }

    // load glyph image into the slot (erase previous one)
    Check(FT_Load_Glyph(*face, glyph_index, FT_LOAD_DEFAULT));

    FT_Glyph glyph;
    Check(FT_Get_Glyph((*face)->glyph, &glyph));

    // Get Bound box of glyph
    FT_Glyph_Get_CBox(glyph, FT_GLYPH_BBOX_PIXELS, &glyph_bbox);

    // Free glyph memory
    FT_Done_Glyph(glyph);

    // Move bound box to current pen location at whole pixel coordinate 
    m->pen->x &= ~0x3F;
    glyph_bbox.xMin += m->pen->x >> 6;
    glyph_bbox.xMax += m->pen->x >> 6;

    // Left to right text doesn't change Y position
    //glyph_bbox.yMin += m->pen->y;
    //glyph_bbox.yMax += m->pen->y;

    // Add character bound box to list
    m->AddBox(glyph_bbox);

    // Add characters bound box to total bound box 
    // Y direction probably doesn't change since it is already set-up for complete line height
    if (glyph_bbox.xMin < m->bbox->xMin)
        m->bbox->xMin = glyph_bbox.xMin;

    if (glyph_bbox.yMin < m->bbox->yMin)
        m->bbox->yMin = glyph_bbox.yMin;

    if (glyph_bbox.xMax > m->bbox->xMax)
        m->bbox->xMax = glyph_bbox.xMax;

    if (glyph_bbox.yMax > m->bbox->yMax)
        m->bbox->yMax = glyph_bbox.yMax;

    // increment pen position
    m->pen->x += (*face)->glyph->advance.x;

    // record current glyph index (for kerning with next character)
    m->previous = glyph_index;
}

static void DrawBitmap(FT_Bitmap* bitmap, Byte* image, int x, int y, int stride, BoundBox box)
{
    // Only Alpha channel is modified to reflect intensity of the rendered pixels of the glyph
    // So all pixels should already be filled with the wanted font color (100% transparent)

    int  i, j;
    int  x_max = x + bitmap->width;
    int  y_max = y + bitmap->rows;

    FT_Byte* src = bitmap->buffer;
    Byte mode = bitmap->pixel_mode;

    int r = 128;
    for (j = y; j < y_max; j++)
    {
        if (r != 128)
        {
            r = 128;
            src++;
        }
        // Point to first pixel at the line to write at the alpha value
        FT_Byte* dst = &image[j * stride + x * 4 + 3];
        for (i = x; i < x_max; i++)
        {
            int n;
            switch (mode) 
            {
            case FT_PIXEL_MODE_GRAY:
                n = *src++;
                break;
            case FT_PIXEL_MODE_MONO:
                n = (*src & r) ? 255 : 0;
                r >>= 1;
                if (r == 0) 
                { 
                    src++; 
                    r = 128; 
                }
                break;
            default:
                n = 0;
                break;
            }
            if ((i >= box.X) && (i < box.X + box.Width)
                && (j >= box.Y) && (j < box.Y + box.Height))
            {
                if (n > *dst) {
                    *dst = n;
                }
            }
            // Move to next pixels alpha
            dst += 4;
        }
    }
}


void ToolsFreeType::FFont::RenderText(BoundBox box, int x, int y, String^ text, int size, IntPtr Buffer, int stride)
{
    if (m_isDisposed) {
        throw gcnew ObjectDisposedException("ToolsFreeType");
    }

    FT_GlyphSlot  slot = (*face)->glyph;
    FT_Vector     pen{};

    FT_UInt       glyph_index;
    FT_Bool       use_kerning;
    FT_UInt       previous;

    Byte* image = (Byte*)Buffer.ToPointer();

    Check(FT_Set_Char_Size(*face, size * 64, 0, dpi, 0));
    pen.x = x * 64;
    pen.y = y;

    use_kerning = FT_HAS_KERNING(*face);
    previous = 0;

    for each (char c in text) {
        // convert character code to glyph index
        glyph_index = FT_Get_Char_Index(*face, c);

        // retrieve kerning distance and move pen position
        if (use_kerning && previous && glyph_index)
        {
            FT_Vector  delta;
            Check(FT_Get_Kerning(*face, previous, glyph_index, FT_KERNING_DEFAULT, &delta));
            pen.x += delta.x;
        }

        // load glyph image into the slot (erase previous one)
        Check(FT_Load_Glyph(*face, glyph_index, FT_LOAD_RENDER));

        // now, draw to our target surface (convert position)
        pen.x &= ~0x3F;
        DrawBitmap(&slot->bitmap, image, (pen.x >> 6) + slot->bitmap_left, pen.y - slot->bitmap_top, stride, box);

        // increment pen position
        pen.x += slot->advance.x;
        pen.y += slot->advance.y >> 6;

        // record current glyph index
        previous = glyph_index;
    }
}
