using namespace Tools::Font;
using namespace System::Collections::Generic;

#pragma once

extern "C" {
    #include <ft2build.h>
    #include FT_FREETYPE_H
    #include FT_GLYPH_H
}

namespace ToolsFreeType {
    ref class Measure : IMeasure
    {
    public:
        Measure() 
        {
            bbox = new FT_BBox;
            pen = new FT_Vector;
        }

        ~Measure() 
        {
            if (m_isDisposed) 
                return;
            this->!Measure();
            m_isDisposed = true;
        }

        !Measure() 
        {
            delete bbox;
            bbox = NULL;
            delete pen;
            pen = NULL;
        }

        virtual IReadOnlyList<BoundBox>^ GetCharacterBoxes();
        virtual BoundBox GetBoundBox();
    internal:
        FT_Bool    use_kerning;
        FT_UInt    previous;
        FT_BBox*   bbox;
        FT_Vector* pen;
        void AddBox(FT_BBox box);
    private:
        bool m_isDisposed;
        List<BoundBox>^ charBoxes = gcnew List<BoundBox>();
    };

}