using namespace System;
using namespace System::IO;
using namespace System::Windows;
using namespace Tools::Font;

#include <ft2build.h>
//#include FT_FREETYPE_H
#include <freetype/freetype.h>
//#include FT_GLYPH_H
#include <freetype/ftglyph.h>

#include "FreeTypeWrapper.h"

namespace ToolsFreeType {
    ref class FFont	: IFontFace
    {
	public:
		FFont(FreeTypeWrapper^ lib, Stream^ fs, int dpi);

		~FFont()
		{
			if (m_isDisposed)
				return;
			this->!FFont();
			m_isDisposed = true;
			System::GC::SuppressFinalize(this);
		}

		!FFont();

		virtual IMeasure^ StartMeasure(int size);
		virtual void MeasureNextChar(IMeasure^ measure, Char c);

		virtual void RenderText(BoundBox box, int x, int y, String^ text, int size, IntPtr Buffer, int stride);

		property String^ FamilyName
		{
			virtual String^ get()
			{
				if (m_isDisposed) {
					throw gcnew ObjectDisposedException("ToolsFreeType");
				}
				if ((*face)->family_name != NULL) {
					return gcnew String((*face)->family_name);
				}
				return String::Empty;
			}
		}

		property String^ StyleName
		{
			virtual String^ get()
			{
				if (m_isDisposed) {
					throw gcnew ObjectDisposedException("ToolsFreeType");
				}
				if ((*face)->style_name != NULL) {
					return gcnew String((*face)->style_name);
				}
				return String::Empty;
			}
		}


		property bool IsBold
		{ 
			virtual bool get()
			{
				if (m_isDisposed) {
					throw gcnew ObjectDisposedException("ToolsFreeType");
				}
				return ((*face)->style_flags & FT_STYLE_FLAG_BOLD) != 0;
			}
		}
		property bool IsItalic
		{ 
			virtual bool get()
			{
				if (m_isDisposed) {
					throw gcnew ObjectDisposedException("ToolsFreeType");
				}
				return ((*face)->style_flags & FT_STYLE_FLAG_ITALIC) != 0;
			}
		}
		
		property double Ascender
		{ 
			virtual double get()
			{
				if (m_isDisposed) {
					throw gcnew ObjectDisposedException("ToolsFreeType");
				}
				return (double)(*face)->ascender * dpi / (72 * (*face)->units_per_EM);
			}
		}
		property double Descender
		{ 
			virtual double get()
			{
				if (m_isDisposed) {
					throw gcnew ObjectDisposedException("ToolsFreeType");
				}
				return (double)(*face)->descender * dpi / (72 * (*face)->units_per_EM);
			}
		}
		property double Height
		{ 
			virtual double get()
			{
				if (m_isDisposed) {
					throw gcnew ObjectDisposedException("ToolsFreeType");
				}
				return (double)(*face)->height * dpi / (72 * (*face)->units_per_EM);
			}
		}
		
		property int DPI
		{
			virtual int get() 
			{
				return dpi;
			}
		}

	private:
		bool m_isDisposed;
		FreeTypeWrapper^ library;
		FT_Face* face;
		FT_Byte* fontData;
		int dpi;

		int ToPixels(short value, int fontSize);
	};
}
