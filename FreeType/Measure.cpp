#include "pch.h"
#include "Measure.h"

IReadOnlyList<BoundBox>^ ToolsFreeType::Measure::GetCharacterBoxes()
{
    return charBoxes;
}

BoundBox ToolsFreeType::Measure::GetBoundBox()
{
    return BoundBox(
        bbox->xMin, 
        bbox->yMin, 
        bbox->xMax - bbox->xMin, 
        bbox->yMax - bbox->yMin);
}

void ToolsFreeType::Measure::AddBox(FT_BBox box)
{
    BoundBox b;
    b.X = box.xMin;
    b.Y = box.yMin;
    b.Width = box.xMax - box.xMin;
    b.Height = box.yMax - box.yMin;
    charBoxes->Add(b);
}
