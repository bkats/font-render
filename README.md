# Font Render

Project to use FreeType to render (simple) texts in .net 6 WPF

## Description

This project wraps FreeType library inside a C/C++ CLR build assembly. Using static libraries from FreeType which can be downloaded [here](https://github.com/ubawurinna/freetype-windows-binaries}). 

To make usage easier there is also a C# assembly which defines the interfaces implemented by the C/C++ classes and is also responsible for loading the 32 or 64 bit version of the build C/C++ assemblies.

Further the C# assembly contains helper functions to render text easier/nicer.

It is limited to simple texts with single line or a few lines using new line character ('\n') or wrapping at max width. Only left to right texts are suppported. Also no unicode control characters supported. It is only a simple renderer to display characters/text.

## Installation

To use these assemblies you need the following files (you need to build 32 and 64 bit version)
 * Tool.Font.dll (C#)
 * FreeType32.dll (C/C++)
 * FreeType64.dll (C/C++)
 * ijwhost.dll 2x (.net 6 helper for C/C++ assemblies)
 * Microsoft C runtime of VS2022
 
When building the project, before you can run the example you have to copy the FreeTypeXX.dll and ijwhost files to the output folder of the example.
output folder should contain:
 * example.exe
 * example.dll
 * tools.font.dll
 * x86\freetype32.dll
 * x86\ijwhost.dll
 * x64\freetype64.dll
 * x64\ijwhost.dll

## Authors and acknowledgment

This project is mostly depending on the work of [FreeType team](https://freetype.org) and also for [ubawurinna](https://github.com/ubawurinna) for building the freetype libraries for Windows.

## License

FreeType is licensed with GPLv2 (see \FreeType\FreeType Info\ for more info)
All other code is MIT licensed.

## Project status

More or less done.