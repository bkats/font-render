using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Tools.Font
{
    public static class FontManager
    {
        static private readonly IFontLoader? fontLoader;
        static private readonly Dictionary<string, IFontFace> fonts = new();
        static private readonly Assembly freeTypeAsm;

        static FontManager() 
        {
            var path = Path.GetDirectoryName(typeof(FontManager).Assembly.Location) ?? string.Empty;
            if (IntPtr.Size == 4)
            {
                try
                {
                    freeTypeAsm = Assembly.LoadFrom(Path.Combine(path, @"x86\FreeType32.dll"));
                }
                catch
                {
                    freeTypeAsm = Assembly.LoadFrom(Path.Combine(path, "FreeType32.dll"));
                }
            }
            else
            {
                try
                {
                    freeTypeAsm = Assembly.LoadFile(Path.Combine(path, @"x64\FreeType64.dll"));
                }
                catch
                {
                    freeTypeAsm = Assembly.LoadFile(Path.Combine(path, "FreeType64.dll"));
                }
            }
            if (freeTypeAsm.CreateInstance("ToolsFreeType.FreeTypeWrapper") is IFontLoader fl) {
                fontLoader = fl;
                fontLoader.SetDPI(96);
            }
        }

        /// <summary>
        /// Load a font into memory after which is can be used to render/draw simple texts
        /// </summary>
        /// <param name="name">Key name to refer to when using the loaded font</param>
        /// <param name="file">The file stream containing the font to load</param>
        /// <returns>True when font could be loaded</returns>
        /// <exception cref="FontRenderException">When FreeType wrapper library could be loaded (freetype32.dll or freetype64.dll)</exception>
        /// <exception cref="ArgumentException">The given name is already used/loaded</exception>
        static public bool LoadFont(string name, Stream file)
        {
            if (fontLoader == null)
            {
                throw new FontRenderException("Font loader not found, missing 'freetype32.dll' or 'freetype64.dll'");
            }
            if (fonts.ContainsKey(name))
            {
                throw new ArgumentException($"A font with {name} is already loaded", nameof(name));
            }

            var font = fontLoader.LoadFont(file);
            if (font == null) 
                return false;

            fonts.Add(name, font);
            return true;
        }

        /// <summary>
        /// Unloads a font revert by name from memory
        /// </summary>
        /// <param name="name">The key name of font to unload</param>
        static public void UnloadFont(string name)
        {
            if (fonts.TryGetValue(name, out var font))
            {
                if (font is IDisposable d)
                {
                    d.Dispose();
                };
                fonts.Remove(name);
            }
        }

        /// <summary>
        /// Checks if the key name is already loaded/used with a font
        /// </summary>
        /// <param name="name">The key name to check</param>
        /// <returns>True when a font using this key name is loaded/available</returns>
        static public bool HasFont(string name)
        {
            return fonts.ContainsKey(name);
        }

        /// <summary>
        /// Return information about the font associated with the given key name
        /// </summary>
        /// <param name="name">The key name to get font info for</param>
        /// <returns>The font info</returns>
        /// <exception cref="FontRenderException">When key name is unknown</exception>
        static public FontInfo GetFontInfo(string name)
        {
            if (fonts.TryGetValue(name, out var font))
            {
                return new FontInfo(font);
            }
            throw new FontRenderException("Font not found");
        }

        /// <summary>
        /// Measure given text/string and return the needed size to render the line of text in given size
        /// The given size can be used with the DrawText function
        /// </summary>
        /// <param name="fontName">The key name of the font to use</param>
        /// <param name="text">Simple single line text to measure (left to right only)</param>
        /// <param name="size">The font size to use</param>
        /// <returns>The size needed to render text</returns>
        /// <exception cref="FontRenderException">When something goes wrong during drawing the text (freetype errors)</exception>
        static public Size MeasureText(string fontName, string text, int size)
        {
            var box = new Size();
            if (fonts.TryGetValue(fontName, out var font))
            {
                box = Measure(text, font, size);
            }
            return box;
        }

        private static Size Measure(string text, IFontFace font, int size)
        {
            var box = new Size();
            var m = font.StartMeasure(size);
            try
            {
                foreach (var c in text)
                {
                    font.MeasureNextChar(m, c);
                }
                var bb = m.GetBoundBox();
                box.Width = bb.Width + 2;
                box.Height = bb.Height + 2;
            }
            finally
            {
                ((IDisposable)m).Dispose();
            }
            return box;
        }

        /// <summary>
        /// Draw single line of text on a bitmap rendered with transparent background and using color for the characters
        /// The given rectangle 'pos' will be completely filled 100% transparent pixels before rendering the text
        /// The text is positioned in left top corner
        /// </summary>
        /// <param name="bitmap">The bitmap to render the text on</param>
        /// <param name="pos">Location to render the text at</param>
        /// <param name="fontName">The key name of the font to use</param>
        /// <param name="text">The single line of text to render (left to right only)</param>
        /// <param name="size">The font size to use</param>
        /// <param name="color">The color to render the text in</param>
        /// <returns>True if successful drawn the text</returns>
        /// <exception cref="ArgumentException">The key name isn't found</exception>
        /// <exception cref="FontRenderException">When something goes wrong during drawing the text (freetype errors)</exception>
        static public bool DrawText(WriteableBitmap bitmap, Rect pos, string fontName, string text, int size, Color color)
        {
            if (fonts.TryGetValue(fontName, out var font))
            {
                if (!new Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight).Contains(pos))
                {
                    throw new ArgumentException("Position is outside bitmap", nameof(pos));
                }

                BoundBox bb = (BoundBox)pos;
                int x = bb.X + 1;
                int y = bb.Y + 1 + (int)(font.Ascender * size);
                bitmap.Lock();
                FillRect(bitmap, color, bb);
                font.RenderText(bb, x, y, text, size, bitmap.BackBuffer, bitmap.BackBufferStride);
                bitmap.AddDirtyRect(new Int32Rect(bb.X, bb.Y, bb.Width, bb.Height));
                bitmap.Unlock();
                return true;
            }
            return false;
        }

        private static unsafe void FillRect(WriteableBitmap bitmap, Color color, BoundBox b)
        {
            var buffer = (int*)bitmap.BackBuffer.ToPointer();
            var stride  = bitmap.BackBufferStride / 4;
            int c = (color.R << 16) | (color.G << 8) | color.B;
            for (int j = 0; j < b.Height; j++)
            {
                for (int i = 0; i < b.Width; i++)
                {
                    buffer[b.X + i + (b.Y + j) * stride] = c;
                }
            }
        }

        /// <summary>
        /// Renders a multi line text to a writeableBitmap. Wrapping lines when they exceed maxWidth (at whole words)
        /// The returned bitmap will have the width of maxWidth and the height is as needed to display the whole text
        /// The text is limited to 30 lines (after wrapping)
        /// </summary>
        /// <param name="text">Simple (multi-line) text to render</param>
        /// <param name="fontName">The key name of the font to use</param>
        /// <param name="size">The font size to use</param>
        /// <param name="color">The color to render the text in</param>
        /// <param name="maxWidth">The width to limit the line length to</param>
        /// <returns>The rendered text as a WriteableBitmap or empty when font isn't found</returns>
        /// <exception cref="FontRenderException">When something goes wrong during drawing the text (freetype errors)</exception>
        static public WriteableBitmap DrawFormatedText(string text, string fontName, int size, Color fontColor, int maxWidth)
        {
            if (fonts.TryGetValue(fontName, out var font))
            {
                int lineHeight = (int)Math.Ceiling(size * 1.6);
                if (font.Height > 0)
                {
                    lineHeight = (int)Math.Ceiling(font.Height * size);
                }

                var lines = GetLines(font, text, size, maxWidth - 2);

                int height = lineHeight * lines.Count + 2;
                var bitmap = new WriteableBitmap(maxWidth, height, 96, 96, PixelFormats.Bgra32, null);
                bitmap.Lock();
                
                FillRect(bitmap, fontColor, new BoundBox(0, 0, maxWidth, height));
                
                var bounds = new BoundBox(0, 0, maxWidth, lineHeight); 
                foreach (var line in lines)
                {
                    int y = bounds.Y + 1 + (int)(font.Ascender * size);
                    font.RenderText(bounds, 1, y, line, size, bitmap.BackBuffer, bitmap.BackBufferStride);
                    bounds.Y += lineHeight;
                }

                bitmap.AddDirtyRect(new Int32Rect(0, 0, maxWidth, height));
                bitmap.Unlock();
                return bitmap;
            }
            return new WriteableBitmap(16, 16, 96, 96, PixelFormats.Bgra32, null);
        }

        private static IReadOnlyList<string> GetLines(IFontFace font, string text, int fontSize, int maxWidth)
        {
            var lines = new List<string>();
            var rawText = new ReadOnlySpan<char>(text.ToCharArray());
            int end = -1;

            // stop when end is reached or when end is 0, than the text doesn't fit in maxWidth
            while ((end != 0) && (end < rawText.Length) && (lines.Count < 30))
            {
                var measure = font.StartMeasure(fontSize);
                try
                {
                    var start = end + 1;
                    end = GetNextLine(font, maxWidth, rawText, start, measure);
                    // Don't add empty lines
                    if (start != end)
                    {
                        lines.Add(rawText[start..end].ToString());
                    }
                }
                finally
                {
                    ((IDisposable)measure).Dispose();
                }
            }
            return lines;
        }

        private static int GetNextLine(IFontFace font, int maxWidth, ReadOnlySpan<char> rawText, int start, IMeasure measure)
        {
            var end = start;
            do
            {
                // Add next word to line
                var nextEnd = end + 1;
                while (nextEnd < rawText.Length
                    && rawText[nextEnd] != ' '
                    && rawText[nextEnd] != '\n'
                    )
                {
                    nextEnd++;
                }

                // Add new part to measure to get whole the line
                var size = MeasureAdd(font, measure, rawText[end..nextEnd]);

                // Line too long? Return previous fitting line end
                if (size.Width > maxWidth)
                {
                    return end;
                }
                end = nextEnd;
                
                // Check for a line-break, return the line end
                if ((end < rawText.Length) && (rawText[end] == '\n'))
                {
                    return end;
                }
            }
            while (end < rawText.Length);
            return end;
        }

        private static BoundBox MeasureAdd(IFontFace font, IMeasure measure, ReadOnlySpan<char> span)
        {
            foreach (var c in span)
            {
                font.MeasureNextChar(measure, c);
            }
            return measure.GetBoundBox();
        }

        /// <summary>
        /// Unloads all fonts from memory
        /// </summary>
        static public void ClearFonts()
        {
            foreach (var font in fonts.Values)
            {
                if (font is IDisposable c)
                {
                    c.Dispose();
                }
            }
            fonts.Clear();
        }
    }
}
