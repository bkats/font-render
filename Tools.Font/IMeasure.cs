﻿using System.Collections.Generic;

namespace Tools.Font
{
    public interface IMeasure
    {
        /// <summary>
        /// Returns the bound box of each character. 
        /// All characters bound boxes are from the pen position (0, 0). 
        /// Note negative Y means character goes below base line 
        /// and positive means the character is floating above. 
        /// </summary>
        /// <returns>All bound boxes for the measured characters</returns>
        IReadOnlyList<BoundBox> GetCharacterBoxes();
        
        /// <summary>
        /// Gets you bound box of all characters so far including initial pen position (0,0) and the complete line height
        /// </summary>
        /// <returns>Needed spaces to draw measured characters, including white spacing for line separation (line height)</returns>
        BoundBox GetBoundBox();
    }
}