﻿using System;
using System.Runtime.Serialization;

namespace Tools.Font
{
    [Serializable]
    public class FontRenderException : Exception
    {
        public FontRenderException()
        {
        }

        public FontRenderException(string? message) : base(message)
        {
        }

        public FontRenderException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected FontRenderException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}