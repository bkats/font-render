﻿namespace Tools.Font
{
    public class FontInfo
    {
        public string FamilyName { get; private set; }
        public string StyleName { get; private set; }
        public bool IsBold { get; private set; }
        public bool IsItalic { get; private set; }

        public FontInfo(IFontFace face)
        {
            FamilyName = face.FamilyName;
            StyleName = face.StyleName;
            IsBold = face.IsBold;
            IsItalic = face.IsItalic;
        }
    }
}
