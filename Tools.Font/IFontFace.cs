﻿using System;

namespace Tools.Font
{
    public interface IFontFace
    {
        /// <summary>
        /// Initializes a new text/character measure. Starting bound box is set to the line height and pen position (0,0) 
        /// </summary>
        /// <param name="size">The font size used to measure characters/text</param>
        /// <returns>Initialize measure with minimum bound box</returns>
        IMeasure StartMeasure(int size);

        /// <summary>
        /// Measures and adds a character to the bound box (positioned after previous character)
        /// </summary>
        /// <param name="measure">Current measure instance</param>
        /// <param name="c">Next character to measure and add to bound box</param>
        void MeasureNextChar(IMeasure measure, char c);

        /// <summary>
        /// Draw a text string on a bitmap (BGRA 32bits) with given size within bound box (normal coordinates on bitmap)
        /// From the location x, y which is the base line for the character. So they go above (-; 'A') and below (+; 'j') the y coordinate.
        /// Drawing over characters only modifies the alpha channel. So the initial bitmap should contain pre-filled pixels with the 
        /// required text color.
        /// </summary>
        /// <param name="bounds">The bound box in which the text draw (clipped) coordinates on bitmap</param>
        /// <param name="x">Baseline position X-coordinate on bitmap (characters go right from here)</param>
        /// <param name="y">Baseline position Y-coordinate on bitmap (characters go around from here)</param>
        /// <param name="text">Simple text with only displayable characters (left to right)</param>
        /// <param name="size">Font size to draw the character at</param>
        /// <param name="Buffer">Pointer to back buffer of a bitmap in format (BGRA 32 bits)</param>
        /// <param name="stride">Bitmap back buffer width of a line in bytes</param>
        void RenderText(BoundBox bounds, int x, int y, string text, int size, IntPtr Buffer, int stride);

        string FamilyName { get; }
        string StyleName { get; }

        bool IsBold { get; }
        bool IsItalic { get; }

        /// <summary>
        /// The height of a the ascender part of this font, to convert to pixels multiply this value with the size
        /// </summary>
        double Ascender { get; }

        /// <summary>
        /// The height of a the descender part of this font, to convert to pixels multiply this value with the size
        /// </summary>
        double Descender { get; }
        
        /// <summary>
        /// The height of a line in this font, to get height in pixels multiply this value with the size
        /// </summary>
        double Height { get; }

        int DPI { get; }
    }
}
