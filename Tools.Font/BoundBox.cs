﻿using System.Windows;

namespace Tools.Font
{
    public struct BoundBox
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public BoundBox(int x, int y, int w, int h) { X = x; Y = y; Width = w; Height = h; }

        public BoundBox(Rect r)
        {
            X = (int)r.X;
            Y = (int)r.Y;
            Width = (int)r.Width;
            Height = (int)r.Height;
        }

        static public explicit operator BoundBox(Rect r)
        {
            return new BoundBox(r);
        }

        static public implicit operator Rect(BoundBox r)
        {
            return new Rect(r.X, r.Y, r.Width, r.Height);
        }

        public override string ToString()
        {
            return $"({X}, {Y}, {Width}, {Height})";
        }
    }
}