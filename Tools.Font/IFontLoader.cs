﻿using System.IO;

namespace Tools.Font
{
    public interface IFontLoader
    {
        IFontFace LoadFont(Stream fontStream);
        void SetDPI(int dpi);
    }
}
